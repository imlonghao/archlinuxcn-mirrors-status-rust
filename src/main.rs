use anyhow;
use futures::future::join_all;
use httpdate::parse_http_date;
use regex;
use reqwest;
use serde_json;
use std::env;
use std::fs::File;
use std::io::Write;
use std::time::{Duration, SystemTime};
use tokio;
use urlparse::urlparse;

mod types;

#[tokio::main]
async fn main() {
    let _guard = sentry::init((
        env::var("DSN").unwrap(),
        sentry::ClientOptions {
            release: sentry::release_name!(),
            ..Default::default()
        },
    ));
    let mut jobs = vec![];
    let mirrorlist = reqwest::get(
        "https://github.com/archlinuxcn/mirrorlist-repo/raw/master/archlinuxcn-mirrorlist",
    )
    .await
    .unwrap()
    .text()
    .await
    .unwrap();
    let re = regex::Regex::new(r"## (.*) \(.*?\) \(.*?\)\n#Server = (.*?)\$arch").unwrap();
    for mirror in re.captures_iter(&mirrorlist[..]) {
        let name = mirror[1].to_string();
        let url = mirror[2].to_string();
        let this_status = get_status(name, url);
        jobs.push(this_status);
    }
    let mut status = join_all(jobs).await;
    let mut newest: u64 = 0;
    for node in &status {
        if node.lastupdate > newest {
            newest = node.lastupdate;
        }
    }
    for node in &mut status {
        node.diff = newest - node.lastupdate;
    }
    println!("{:?}", status);
    write_json(&status, "/home/imlonghao/public_html/status/status.json").unwrap();
    write_influxdb(&status).await.unwrap();
}

async fn get_status(name: String, url: String) -> types::StatusElement {
    let mut count: u32 = 0;
    loop {
        let lastupdate = get_lastupdate(&url).await.unwrap_or(0);
        if lastupdate == 0 && count < 2 {
            count += 1;
            continue;
        }
        return types::StatusElement {
            name,
            url,
            lastupdate,
            diff: 0,
        };
    }
}

async fn get_lastupdate(url: &String) -> Result<u64, anyhow::Error> {
    let client = reqwest::Client::builder()
        .timeout(Duration::from_secs(5))
        .user_agent("Mozilla/5.0 ArchlinuxCN Mirror Checker")
        .build()?;
    let resp = client
        .head(&format!("{}x86_64/archlinuxcn.db", url))
        .send()
        .await?;
    let lastmodified = match resp.headers().get("last-modified") {
        Some(r) => r.to_str()?,
        None => anyhow::bail!("last-modified for {} is none", url),
    };
    let ts = parse_http_date(lastmodified)?
        .duration_since(SystemTime::UNIX_EPOCH)?
        .as_secs();
    Ok(ts)
}

fn write_json(status: &std::vec::Vec<types::StatusElement>, path: &str) -> std::io::Result<()> {
    let json = serde_json::to_string(status).unwrap();
    let mut file = File::create(path)?;
    file.write_all(json.as_bytes())?;
    Ok(())
}

async fn write_influxdb(
    status: &std::vec::Vec<types::StatusElement>,
) -> Result<(), reqwest::Error> {
    let mut inf = String::new();
    for node in status {
        if node.lastupdate == 0 {
            continue;
        }
        let url = urlparse(&node.url);
        let domain = url.hostname.unwrap();
        inf.push_str(&format!("mirrors,host={} value={}\n", domain, node.diff)[..]);
    }
    inf.pop();
    let client = reqwest::Client::new();
    client
        .post("https://influxdb.esd.cc/write?db=archcn")
        .body(inf)
        .basic_auth(env::var("USER").unwrap(), Some(env::var("PASS").unwrap()))
        .send()
        .await?;
    Ok(())
}

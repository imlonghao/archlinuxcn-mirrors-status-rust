use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct StatusElement {
    pub name: String,
    pub url: String,
    pub lastupdate: u64,
    pub diff: u64,
}
